package rocks.turncodr.mycurriculum.rest;

import java.util.List;

import org.springframework.hateoas.RepresentationModel;

public interface IBasicRestController<T extends RepresentationModel<T>> {
	void addHateoasToResource(List<T> resources);
}
