package rocks.turncodr.mycurriculum.rest;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import rocks.turncodr.mycurriculum.model.AreaOfStudies;
import rocks.turncodr.mycurriculum.model.Curriculum;
import rocks.turncodr.mycurriculum.model.ExReg;
import rocks.turncodr.mycurriculum.model.Module;
import rocks.turncodr.mycurriculum.services.ModuleJpaRepository;

/**
 * Module Rest controller.
 *
 */
@RestController
@RequestMapping(value = "/module")
public class ModuleRestController implements IBasicRestController<Module>{

    @Autowired
    private ModuleJpaRepository moduleJpaRepository;
    
    @Autowired
    private AreaOfStudiesRestController areaOfStudiesRestController;
    
    @Autowired
    private CurriculumRestController curriculumRestController;
    
    @Autowired
    private ExRegRestController exRegRestController;

    @RequestMapping("/details/{id}")
    @ResponseStatus(value = HttpStatus.OK)
    public String getModuleDetails(@PathVariable("id") String id, Model model) {
        Module module = moduleJpaRepository.getOne(Integer.parseInt(id));
        model.addAttribute("module", module);
        return "/fragments/popupModuleOverview :: showModuleDetails";
    }
    
	@GetMapping("/{idFromUrl}")
	public ResponseEntity<List<Module>> getModules(@PathVariable Integer... idFromUrl) {
		List<Integer> ids = idFromUrl != null ? Arrays.asList(idFromUrl) : new ArrayList<>();
		List<Module> modules = moduleJpaRepository.findAllById(ids);
		
		if (modules.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		addHateoasToResource(modules);
		return ResponseEntity.ok(modules);
	}
	
	@GetMapping()
	public List<Module> getModulesByAttribute(
			@RequestParam(value = "title", required = false, defaultValue = "") List<String> title,
			@RequestParam(value = "exregid", required = false, defaultValue = "") List<Integer> exregId,
			@RequestParam(value = "coordinator", required = false, defaultValue = "") List<String> coordinator,
			@RequestParam(value = "credits", required = false, defaultValue = "") List<Integer> credits,
			@RequestParam(value = "teachingLanguage", required = false, defaultValue = "") List<String> teachingLanguage) {
		List<Module> modules = new LinkedList<>();
		if (title.isEmpty() && exregId.isEmpty() && coordinator.isEmpty() && credits.isEmpty() && teachingLanguage.isEmpty()) {
			modules.addAll(moduleJpaRepository.findAll());
			addHateoasToResource(modules);
			return modules;
		}
		modules.addAll(moduleJpaRepository.findByCreditsIn(credits));
		modules.addAll(moduleJpaRepository.findByExRegIn(exRegRestController.getExregsById(exregId.toArray(new Integer[exregId.size()]))));
		modules.addAll(moduleJpaRepository.findByTitleIn(title));
		modules.addAll(moduleJpaRepository.findByTeachingLanguageIn(teachingLanguage));
		addHateoasToResource(modules);
		return modules;
	}
	
//	@GetMapping("/areaofstudies")
//	public List<Module> getModulesByAreaOfStudies(
//			@RequestParam(name = "id", required = false, defaultValue = "") List<Integer> urlIds,
//			@RequestParam(name = "name", required = false, defaultValue = "") List<String> areaOfStudiesName) {
//		List<Module> foundModules = new LinkedList<>();
//		if (!urlIds.isEmpty()) {
//			foundModules = moduleJpaRepository.findByAreaOfStudiesIn(areaOfStudiesRestController.getAreaOfStudies(urlIds.toArray(new Integer[urlIds.size()])));
//		}
//		foundModules.addAll(moduleJpaRepository.findByAreaOfStudiesIn(areaOfStudiesRestController.getAreaOfStudiesByName(areaOfStudiesName)));
//		addHateoasToResource(foundModules);
//		return foundModules;
//	}
	
    @PostMapping()
    public ResponseEntity<Module> postModuleCreate(@Valid @RequestBody Module module) {
        Module savedModule = moduleJpaRepository.save(module);
        addHateoasToResource(Arrays.asList(module));
        return ResponseEntity.ok(savedModule);
    }
    

	@Override
	public void addHateoasToResource(List<Module> resources) {
		resources.forEach(module -> {
			Link link = linkTo(methodOn(ModuleRestController.class).getModules(module.getId())).withSelfRel();
	    	module.add(link);
	    	//^- create and add links for the module
	    	AreaOfStudies aos = module.getAreaOfStudies();
	    	if (aos.getLinks().isEmpty()) {
	    		link = linkTo(methodOn(AreaOfStudiesRestController.class).getAreaOfStudies(aos.getId())).withSelfRel();
	    		aos.add(link);
	    	}
	    	ExReg exreg = module.getExReg();
	    	if (exreg != null && !exreg.hasLinks()) {
	    		link = linkTo(methodOn(ExRegRestController.class).getExregsById(exreg.getId())).withSelfRel();
	    		exreg.add(link);
	    		
	    		Curriculum curriculum = exreg.getCurriculum();
	    		if (curriculum != null && curriculum.hasLinks() == false) {
	    			link = linkTo(methodOn(CurriculumRestController.class).getCurriculumList(exreg.getId())).withSelfRel();
	    			curriculum.add(link);
	    		}
	    	}
		});
	}
}
