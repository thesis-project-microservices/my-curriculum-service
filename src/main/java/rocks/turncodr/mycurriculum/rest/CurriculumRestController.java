package rocks.turncodr.mycurriculum.rest;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import rocks.turncodr.mycurriculum.model.Curriculum;
import rocks.turncodr.mycurriculum.services.CurriculumJpaRepository;

/**
 * Controller class directing to the about page of the application.
 *
 */
@RestController
@RequestMapping("/curriculum")
public class CurriculumRestController implements IBasicRestController<Curriculum>{
    @Autowired
    private CurriculumJpaRepository curriculumJpaRepository;

    @GetMapping("/{idFromUrl}")
    public List<Curriculum> getCurriculumList(@PathVariable(required = false) Integer... idFromUrl) {
    	List<Integer> ids = idFromUrl != null ? Arrays.asList(idFromUrl) : new ArrayList<>();
    	List<Curriculum> curricula;
    	if (ids.isEmpty()) {
    		curricula = curriculumJpaRepository.findAll();
    	} else {  		
    		curricula = curriculumJpaRepository.findAllById(ids);
    	}
    	addHateoasToResource(curricula);
        return curricula;
    }
    
    @GetMapping()
    public List<Curriculum> getCurriculumListByAttribute(
    		@RequestParam(name = "name", required = false, defaultValue = "") List<String> curriculumNames,
    		@RequestParam(name = "degree", required = false, defaultValue = "") List<String> curriculumDegrees) {
    	List<Curriculum> curricula = curriculumJpaRepository.findAllByNameIn(curriculumNames);
    	curricula.addAll(curriculumJpaRepository.findAllByDegreeIn(curriculumDegrees));
    	addHateoasToResource(curricula);
    	return curricula;
    }

    @PostMapping()
    public Curriculum curriculumSubmit(@Valid @RequestBody Curriculum curriculum) {
        return curriculumJpaRepository.save(curriculum);
    }

	@Override
	public void addHateoasToResource(List<Curriculum> resource) {
		resource.forEach(res-> {
			Link link = linkTo(methodOn(CurriculumRestController.class).getCurriculumList(res.getId())).withSelfRel();
			res.add(link);	
		});
	}
}
