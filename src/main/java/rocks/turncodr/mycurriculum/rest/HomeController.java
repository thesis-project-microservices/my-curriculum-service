package rocks.turncodr.mycurriculum.rest;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

@RestController("/")
public class HomeController {
	
	@Autowired
	private RequestMappingHandlerMapping requestHandlerMapping;

	@GetMapping()
	public ResponseEntity<Object> home() {
		Map<String, Object> response = new LinkedHashMap<>();
		response.put("name", "my-curriculum-service");
		response.put("message", "This is the my-curriculum Service 'Home' site.");
		response.put("endpoints", requestHandlerMapping.getHandlerMethods().keySet().stream().map(reqInfo -> reqInfo.getPatternsCondition()).distinct().collect(Collectors.toList()));
		return ResponseEntity.ok(response);
	}
}
