package rocks.turncodr.mycurriculum.rest;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import rocks.turncodr.mycurriculum.model.AreaOfStudies;
import rocks.turncodr.mycurriculum.services.AreaOfStudiesJpaRepository;
import rocks.turncodr.mycurriculum.validation.AreaOfStudiesValidator;

@RestController
@RequestMapping(value = "/areaofstudies")
@Validated
public class AreaOfStudiesRestController implements IBasicRestController<AreaOfStudies>{

	@Autowired
	AreaOfStudiesJpaRepository areaOfStudiesRepository;
	
	@Autowired
    AreaOfStudiesValidator colorValidator;
	
	@GetMapping("/{idFromUrl}")
	public List<AreaOfStudies> getAreaOfStudies(@PathVariable Integer... idFromUrl) {
		List<Integer> ids = idFromUrl != null ? Arrays.asList(idFromUrl) : new ArrayList<>();
		List<AreaOfStudies> areaOfStudies = areaOfStudiesRepository.findAllById(ids);
		areaOfStudies.forEach(aos -> {
			Link link = linkTo(methodOn(AreaOfStudiesRestController.class).getAreaOfStudies(aos.getId())).withSelfRel();
			aos.add(link);
		});
				
		return areaOfStudies;
	}
	
	@GetMapping("")
	public List<AreaOfStudies> getAreaOfStudiesByName(@RequestParam(value = "name", required = false, defaultValue = "") List<String> name) {
		List<AreaOfStudies> areaOfStudies = new LinkedList<>();
		if (!name.isEmpty()) {
			areaOfStudies.addAll(areaOfStudiesRepository.findByNameIn(name));
			addHateoasToResource(areaOfStudies);
			return areaOfStudies;
		}
		areaOfStudies.addAll(areaOfStudiesRepository.findAll());
		addHateoasToResource(areaOfStudies);
		return areaOfStudies;
	}
	
	@PostMapping(consumes = "application/json", produces="application/json")
	public ResponseEntity<Object> createAreaOfStudies(@RequestBody @Valid AreaOfStudies areaOfStudies, Errors errors) {
		areaOfStudies.setColorRGB(intToRGB(areaOfStudies.getColor()));
		colorValidator.validate(areaOfStudies, errors);
		
		if (errors.hasErrors()) {
			Map<String, Object> body = new LinkedHashMap<>();
			body.put("timestamp", new Date());
			body.put("status", HttpStatus.BAD_REQUEST.value());
			body.put("errors", errors.getAllErrors().stream().map(e -> e.getCode()).collect(Collectors.toList()));
			return ResponseEntity.badRequest().body(body);
		}
		return ResponseEntity.ok(areaOfStudiesRepository.save(areaOfStudies));
	}
	
    /**
     * Translates the color back to RGB representation and returns it as a string.
     *
     * @param colorAsInt The color as integer
     * @return r = red component, g = green component, b = blue component
     */
    @SuppressWarnings("checkstyle:magicnumber")
    private String intToRGB(int colorAsInt) {
        colorAsInt >>>= 0;
        int b = colorAsInt & 0xFF, g = (colorAsInt & 0xFF00) >>> 8, r = (colorAsInt & 0xFF0000) >>> 16;
        return r + "," + g + "," + b;
    }

	@Override
	public void addHateoasToResource(List<AreaOfStudies> resources) {
		resources.forEach(res -> {
			Link link = linkTo(methodOn(AreaOfStudiesRestController.class).getAreaOfStudies(res.getId())).withSelfRel();
			res.add(link);
		});
	}
}
