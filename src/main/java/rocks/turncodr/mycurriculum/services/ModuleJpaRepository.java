package rocks.turncodr.mycurriculum.services;

import org.springframework.data.jpa.repository.JpaRepository;

import rocks.turncodr.mycurriculum.model.AreaOfStudies;
import rocks.turncodr.mycurriculum.model.ExReg;
import rocks.turncodr.mycurriculum.model.Module;

import java.util.List;

/**
 * JPARepository for Modules.
 */

public interface ModuleJpaRepository extends JpaRepository<Module, Integer>  {
    List<Module> findByExReg(ExReg exReg);
    List<Module> findByAreaOfStudiesIn(List<AreaOfStudies> areaOfStudies);
    List<Module> findByExRegIn(List<ExReg> exReg);
    List<Module> findByTitleIn(List<String> title);
    List<Module> findByCreditsIn(List<Integer> credits);
    List<Module> findByTeachingLanguageIn(List<String> language);
}
