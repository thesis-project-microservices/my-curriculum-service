package rocks.turncodr.mycurriculum.services;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import rocks.turncodr.mycurriculum.model.Curriculum;
import rocks.turncodr.mycurriculum.model.ExReg;

import java.sql.Date;
import java.util.Collection;
import java.util.List;

/**
 * JpaRepository for Examination Regulations (ExReg).
 */
@Repository
public interface ExRegJpaRepository extends JpaRepository<ExReg, Integer> {
    List<ExReg> findByCurriculum(Curriculum curriculum);
    @Query(nativeQuery = true, value="SELECT * FROM Exreg WHERE curriculum_id IN :curriculum")
    List<ExReg> findAllByCurriculum(Collection<Curriculum> curriculum);
    List<ExReg> findAllByValidFromIn(Collection<Date> date);
    List<ExReg> findAllByNumberOfSemestersIn(Collection<Integer> nrSemester);
}
