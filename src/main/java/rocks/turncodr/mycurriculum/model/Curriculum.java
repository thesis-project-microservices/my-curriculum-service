package rocks.turncodr.mycurriculum.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

import org.springframework.hateoas.RepresentationModel;

/**
 * Entity curriculum.
 */
@Entity
public class Curriculum extends RepresentationModel<Curriculum>{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Valid
    @NotEmpty(message="{curriculum.error.empty.acronym}")
    private String acronym;
    @Valid
    @NotEmpty(message="{curriculum.error.empty.name}")
    private String name;
    @Valid
    @NotEmpty(message="{curriculum.error.empty.degree}")
    private String degree;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getAcronym() {
        return acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }
}
