package rocks.turncodr.mycurriculum.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import rocks.turncodr.mycurriculum.error.LoginAccessDeniedHandler;

/**
 * Spring securtiy configuration class.
 * Handles all non-secured and secured pages.
 */
@Configuration
@EnableOAuth2Sso
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    LoginAccessDeniedHandler accessDeniedHandler;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().antMatchers(
                "/home",
                "/curriculum/list",
                "/exreg/list",
                "/exreg/overview",
                "/exreg/syllabus",
                "/exreg/syllabus/pdf",
                "/module/list",
                "/module/details/**",
                "/css/**",
                "/img/**",
                "/js/**",
                "/fragments/**",
                "/webfonts/**",
                "/",
                "/login").permitAll()
        		.antMatchers(HttpMethod.POST,"/module", "/areaofstudies", "/curriculum", "/exreg").hasRole("ADMIN")
        		.antMatchers(HttpMethod.PUT,"/module", "/areaofstudies", "/curriculum", "/exreg").hasRole("ADMIN")
                .and().csrf().disable()
                .logout().logoutSuccessUrl("http://localhost:8090/auth/exit")
        		.and()
                .exceptionHandling()
                .accessDeniedHandler(accessDeniedHandler);
    }
}
