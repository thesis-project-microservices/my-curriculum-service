package rocks.turncodr.mycurriculum.error;

import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

@RestController
public class MyErrorController extends DefaultErrorAttributes implements ErrorController {

	@Override
	public String getErrorPath() {
		return "/error";
	}

	@RequestMapping("/error")
	public ResponseEntity<Object> handleError(HttpServletRequest request, WebRequest webRequest) {
		Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
		Map<String, Object> errorAttributes = super.getErrorAttributes(webRequest, true);
		return ResponseEntity.status(HttpStatus.valueOf((Integer) status)).body(errorAttributes);
	}
}
