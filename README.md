# my-curriculum-service
Backend service for managing the curriculum and syllabi handbooks of degree programs with Spring Boot

## Getting Started
The following steps will take you to the start:
The prerequisite to work on this project is to install JDK 11 (or later) on your system.

Next install the project building tool Maven.

If you want to try out this application you'll need the version control tool git.

If you don't have a GitLab account yet, create one [here](https://gitlab.com/) or use your github account.

Clone the repository via `git clone https://gitlab.com/thesis-project-microservices/my-curriculum-service.git`

Add the remote repository *upstream* via `git remote add upstream https://gitlab.com/thesis-project-microservices/my-curriculum-service.git`

Start the application by navigating to the project folder and executing `./mvnw spring-boot:run`

If you want to post data to the secured endpoints, make sure you are running the authentication service and received the authentication cookie.<br>
The Authentication Service can be found [here](https://gitlab.com/thesis-project-microservices/authentication-service)<br>
<b>BEAWARE</b> the my-curriculum Service needs to run with the Gateway-API, otherwise it <b>can't</b> find the auth Service and your authentication will not work.<br>
it can be found [here](https://gitlab.com/thesis-project-microservices/gateway-api)

### Acquiring the auth cookie

You can acquire the auth cookie from the auth service by entering the user credentials in the login form.<br>
The login form can be reached under the url `localhost:8090/auth/login` it's the standard login form shipped with spring-security.

The user credentials are:
<table>
	<tr><th>Username</th><th>Password</th></tr>
	<tr><td>admin</td><td>123</td></tr>
</table

